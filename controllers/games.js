var PlayerStatus        = require('../models/playerStatus.js');
var Game                = require('../models/game.js');
var User                = require('../models/user.js');
var Card                = require('../models/card.js');
var GameService         = require('../services/gameService.js');
var PlayerStatusService = require('../services/playerStatusService');
var TransactionService  = require('../services/transactionService');

exports.index = function (req, res) {
  User.findOne({id: req.session.user}, function (err, user) {
    Game.find({}, function (err, games) {
      if (err) return err;
      res.render('games/index', {games:games, session: req.session});
    });
  });
};

exports.new = function(req, res) {
  res.render('games/new', {title: 'New Game', session: req.session});
};

exports.show = function(io, roomPatrons) {
  return function(req, res) {
    var game_id = req.url.split('/')[2];
    
    Game.findById(game_id, function(err, game) {
      if (err) return err;
      var playerNames = _und.pluck(game.players, 'firstName').join(', ');
      var me          = _und.select(game.players, function(player) {return player.user_id == req.session.user_id;})[0] || '';
      res.render('games/show', {game: game, session: req.session, patrons: roomPatrons, playerNames: playerNames, me: me});
    });
  };
};

exports.create = function(req, res) {
  var game = new Game({title: req.body.game.title});
  game.save(function(err) {
    if (!err)
      res.redirect('/games');
    else
      console.error('Error: Could not save' + err);
  });
};

exports.start = function(config) {
  return function(req, res) {
    this.game_id = req.body.game_id;
    this.user    = req.session.user;
    this.patrons = req.body.data;
    
    var self = this;

    Game.findById(game_id, _und.bind(function(err, game) {
      if (err) return err;

      game.start(patrons, _und.bind(function() {

        io.sockets.clients(game.id).forEach(function(socket) {
            var p = _und.select(game.players, function(player){return player.user_id === socket.user.id;})[0];
            
            var gameStatus = {
              title:       game.title,
              cards:       game.cards,
              isActive:    game.isActive,
              isWon:       game.isWon,
              winner:      game.winner,
              playerNames: _und.map(game.players, function(player){return player.firstName;}),
              player: p
            };

            socket.emit('update', gameStatus);
        });

        res.writeHead(200, { 'Content-Type': 'application/json'});
        res.end();
      
      }), self);
      
    }, this));
  };
};

// get the player
// check if player's token matches game token
// get the cards from the game
// get the cards from the player
// if treasureCards' cost > cards cost, then exchange
// then call update
exports.purchase = function(req, res) {
  this.gameId          = req.body.gameId;
  this.userId        = req.body.userId;
  this.card            = req.body.card;
  this.treasureCardIds = req.body.treasureCardIds;
  
  Game.findById(this.gameId, _und.bind(function(err, game) {
    if (err) return err;
    
    var player, idx, isVerified, verifiedCards, hasMoney;
    
    player = _und.where(game.players, {user_id: this.userId})[0];
    idx    = game.players.indexOf(player);
    player = new PlayerStatus(player);
    
    isVerified    = game.verifyToken(player.getToken());
    verifiedCards = [];
    

    if (isVerified) {

      // need to convert cards to Card mongoose 
      var hand = _und.map(player.hand, function(card) {return new Card(card);});
      
      // check if cards are actually in player's hand
      verifiedCards = PlayerStatusService.getVerifiedCards(hand, this.treasureCardIds);
      
      // check if player can afford it
      hasMoney = TransactionService.validate(verifiedCards, this.card, player.buys);
      
      // if the sum equals the total value of the treasure cards, then process
      if (hasMoney) {
        player = TransactionService.transact(verifiedCards, this.card, player, game);
        player.buys--;
        
        GameService.updatePlayer(game, player, idx);

        game.save(function(err, g) {
          if (err) throw err;
          
          // a. TODO - send a message of the move. -- something like new Message()
          // b. update the ui through socket.io
          GameService.updatePlayerSockets(g, player, io);
        });
      }
    }
    else {
      console.log("NOT THE RIGHT TURN TOKEN");
      console.log("player turntoken: "+ player.turnTokens[player.turnTokens.length-1]);
      console.log("game turntoken:"+game.turnTokens[game.turnTokens.length-1]);
    }
   }), this);

  
  res.writeHead(200, { 'Content-Type': 'application/json'});
  res.end();
};

// 1. takes the hand and adds them to the discard pile.
// 2. if there are no more cards left in the deck, add the discard
// pile to the deck and shuffle them.
// 3. Draw 5 new cards for the hand.
// 4. Player gets assigned a new turn token 
exports.cleanup = function(req, res) {
  this.player = req.body;
  this.userId  = req.body.user_id; 
  this.gameId = req.url.split('/')[2];
  var self    = this;
  
  Game.findById(this.gameId, function(err, game) {
    if (err) return err;
    
    var player, isVerified, idx;
    
    player     = _und.where(game.players, {user_id: self.userId})[0];
    idx        = game.players.indexOf(player);
    player     = new PlayerStatus(player);
    isVerified = GameService.verifyToken(game, player.getToken() );
    
    if (isVerified) {
      
      // transfer hand to discard pile
      player = PlayerStatusService.discardHand(player);
      
      // if there are less than 5 remaining in the deck, add cards from the 
      // discard pile to the deck
      if (player.deck.length < 5) {
        player = PlayerStatusService.restoreDeck(player);
      }

      if (player.hand.length > 0) {
        player.disposeCards(player.hand.length);
      }

      // reset buys
      player.buys = 1;
      
      player.createHand(5);
      game.assignNextTurn();
      game.players[idx] = player;
      game.markModified('players');
      
      game.save(function (err, game) {
        if (err) throw err;
        
        GameService.updatePlayerSockets(game, player, io);
      });
    }
    

  });
  
  res.writeHead(200, { 'Content-Type': 'application/json'});
  res.end();
    
};

