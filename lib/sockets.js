module.exports = function(config) {
  var io          = config.io,
      roomPatrons = config.roomPatrons,
      roomSockets = config.roomSockets;

  io.sockets.on('connection', function(socket) {
    console.log('--------------- CONNECTED! --------------------');
    
    ids = _und.pluck(_und.flatten(roomPatrons), 'id');

    // we join a room once we enter a game's URL, e.g. games/:id
    socket.on('joinroom', function(data) {
      socket.user = data.user;
      if (!data.user.id) return;
      
      // to prevent from entering more than once
      if (!_und.contains(ids, data.user.id)) {
        
        // add them to the room if room exists
        // otherwise create the room and then add them
        if (!roomPatrons[data.game])
          roomPatrons[data.game] = [];
        
        roomPatrons[data.game].push(data.user);
        
        // save the socket id for communication 
        if (!roomSockets[data.game])
          roomSockets[data.game] = [];
        
        roomSockets[data.game].push(socket.id);
      }

      // config.roomPatrons = roomPatrons;
      socket.join(data.game);
      socket.room = data.game;
      io.sockets.in(data.game).emit('updatechat',roomPatrons[data.game]);
    });

    socket.on('playcard', function(data) {
      console.log(data);
    });



    // Removes a user from the roomPatrons array if they disconnect
    socket.on('disconnect', function() {
      console.log('------------ DISCONNECTED! --------------------');
      if (socket.user && socket.room) {
        patrons = roomPatrons[socket.room];
        _und.each(patrons, function(patron) {
          if (patron.id === socket.user.id) {
            var i = patrons.indexOf(patron);
            patrons.splice(i, 1);
            io.sockets.in(socket.room).emit('updatechat', patrons);
          }
        });
      }
    });

  });

};