// card model

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var cardSchema = new Schema({
  name:         String,
  cardType:     String,
  cost:         Number,
  description:  String,
  value:        Number
});

cardSchema.statics = {
  getSchema: function() {
    return this.cardSchema;
  }
};

cardSchema.methods = {

  rule: function() {
    switch (this.name) {
      case 'Market':
        // do this
        break;
      case 'Mine':
        // do this
        break;
    }
    return "I got to rule!";
  }
};

module.exports = mongoose.model('Card', cardSchema);