// Game model
var Card         = require('./card.js');
var PlayerStatus = require('./playerStatus.js');
var _und         = require('underscore');

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var gameSchema = new Schema({
  title:         String,
  maxNumPlayers: {type: Number,  default: 4},
  players:       [ PlayerStatus.getSchema() ],
  cards:         [ Card.getSchema() ],
  isActive:      {type: Boolean, default: false},
  isWon:         {type: Boolean, default: false},
  winner:        {type: Object,  default: null},
  turnTokens:    {type: Array,   default: null},
  turnIndex:     {type: String,  default: 0}
});

gameSchema.methods = {
  
  start: function(patrons, callback) {
    var token;
    
    if (!this.isActive) {

      // 1. creates a playerStatus for every user
      this.createPlayerStatuses(patrons);

      // 2. creates the required number of cards
      this.createCards();
      
      // 3. creates the starting decks for each player
      this.createDecks();

      // 4. create the starting hands for each player
      this.createHands();

      // 5. create a turn token for the a player
      token = this.createTurnToken(25);

      // 6. assign token to the first player
      this.assignTurnToken(token, this.players[0]);

      this.isActive = true;

      this.save(function(err) {
        if (err) throw err;
      });
    }
    callback();
  },

  // creates a token that allows the player to take a turn.
  // Takes the length as a parameter
  createTurnToken: function(len) {
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
        resp  = '';

    for (var i = 0; i < len; i++)
      resp += chars.charAt(Math.floor(Math.random() * chars.length) );

    return resp;
  },

  // creates a turn token for a player 
  assignTurnToken: function(token, player) {
    player.turnTokens.push(token);
    this.turnTokens.push(token);
    // this.save();
  },

  // assigns next turn to a player
  assignNextTurn: function() {
    var token, idx;
    
    idx   = this.updateTurnIndex();
    token = this.createTurnToken(25);
    this.assignTurnToken(token, this.players[idx]);
    return this.players[idx];
  },

  updateTurnIndex: function() {
    this.turnIndex++;

    if (parseInt(this.turnIndex) === this.players.length) {
      this.turnIndex = 0;
    }
    return this.turnIndex;
  },

  // determines whether the token passed to it matches the game's token
  verifyToken: function(token) {
    return token === this.turnTokens[this.turnTokens.length-1];
  },

  // Creates the starting deck for each player 
  // Note we're converting an array of PlayerStatus schema objects that's 
  // persisted in the database to a PlayerStatus object. (Mongo stores only
  // the schema, not the model )
  createDecks: function() {
    var copper  = _und.where(this.cards, {name: 'Copper'});
    var estates = _und.where(this.cards, {name: 'Estate'});
    var self    = this;

    _und.each(this.players, function(player) {
      
      // add 7 coppers
      for (var i = 0; i < 7; i++) {
        player.addCard( copper.pop() );
      }
      // add 3 estates
      for(var j = 0; j < 3; j++) {
        player.addCard( estates.pop() );
      }

      // shuffle
      player.shuffleDeck();

    });
  },

  // creates hands for all players
  createHands: function() {
    var len = this.players.length;
    if (len === 0) return false;
    
    for (var i = 0; i < len; i++) {
      this.players[i].createHand(5);
    }
  },
  
  // The Game creates PlayerStatus objects for the 1st 
  // n of players.
  createPlayerStatuses: function(patrons) {
    limit = Math.min(this.maxNumPlayers, patrons.length);
    for(var i = 0; i < limit; i++)
      this.createPlayerStatus(patrons[i]);
  },
  
  // each player has a PlayerStatus object the Game 
  // users to interact with the User 
  // Checks to make sure the player hasn't already been
  // added. 
  createPlayerStatus: function(patron) {
    var self = this;
    if (!this.isPlayer(patron) ) {
      var playerStatus = new PlayerStatus({
        user_id:   patron.id,
        firstName: patron.firstName,
        user:      patron,
        game_id:   this.id,
        buys:      1,
        actions:   1
      });
      
      this.players.push(playerStatus);
      
    }
  },

  // Checks whether a patron is listed as as player.
  isPlayer: function(patron) {
    player_ids = _und.pluck(this.players, 'user_id');
    return _und.contains(player_ids, patron.id);
  },

  // creates the necessary cards for the game.
  createCards: function() {
    this.createValueCards('Copper', 1, 'Treasure', 0, 60);
    this.createValueCards('Silver', 2, 'Treasure', 3, 40);
    this.createValueCards('Gold', 3, 'Treasure', 6, 30);

    this.createValueCards('Estate', 1, 'Victory', 2, 24);
    this.createValueCards('Duchy', 3, 'Victory', 5, 12);
    this.createValueCards('Province', 6, 'Victory', 8, 6);

    this.createActionCards();
   },

   // creates the required number of action cards
  createActionCards: function() {
    var desc;
    
    this.createKingdomCards('Market','Action',5, '+1 Card +1 Action +1 Buy', 10);
    
    desc = 'Trash a treasure card from your hand. Gain a Treasure card costing up to 3 pieces more; put it int your hand';
    this.createKingdomCards('Mine','Action',5,desc, 10);

    desc = 'Trash a card from your hand. Gain a card costing up to 2 pieces more than the trashed card.';
    this.createKingdomCards('Remodel','Action',4,desc,10);

    desc = '+2 pieces. Each other player discards down to 3 cards in his hand';
    this.createKingdomCards('Militia','Action-Attack',4,desc,10);

    desc = '+2 cards. When another player pays an Attack card, you may reveal this from your hand. If you do, you are unaffected by that Attack';
    this.createKingdomCards('Moat','Action-Reaction',2,desc,10);

    desc = '+3 cards';
    this.createKingdomCards('Smithy','Action',4,desc,10);

    desc = '+1 Card 2+ Actions';
    this.createKingdomCards('Village','Action',3,desc,10);

    desc = '+1 Action. Discard any number of cards. +1 Card per card discarded.';
    this.createKingdomCards('Cellar','Action',2,desc,10);

    desc = 'Gain a card costing up to 4 pieces.';
    this.createKingdomCards('Workshop','Action',3,desc,10);
  },

  // creates either Treasure or Estate cards
  createValueCards: function(name, value, type, cost, limit) {
    for (var i = 0; i < limit; i++) {
      
      var card = new Card({
        name: name,
        cardType: type,
        cost: cost,
        value: value
      });

      card.save();
      this.cards.push(card);
    }
  },

  // creates action cards
  createKingdomCards: function(name, type, cost, description, limit) {
    for (var i = 0; i < limit; i++) {
      var card = new Card({
        name: name,
        cardType: type,
        cost: cost,
        description: description
      });
    
      card.save();
      this.cards.push(card);
    }
  }

};

module.exports = mongoose.model('Game', gameSchema);