// Player Status model
var Card = require('./card.js');

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var playerStatusSchema = new Schema({
  // user:    Object,
  user_id:   String,
  firstName: String,
  game_id:   String,
  deck:      [Card.getSchema() ],
  hand:      [Card.getSchema() ],
  discard:   [Card.getSchema() ],
  turnTokens: Array,
  buys:       Number,
  actions:    Number,

});

playerStatusSchema.methods = {

  turn: function() {
    return this.turnTokens[this.turnTokens.length-1];
  },
  
  playCard: function(card) {
    if (!card instanceof Card) return false;
  },

  addCard: function(card) {
    if (!card instanceof Card) return false;
    this.deck.push(card);
  },

  shuffleDeck: function() {
    this.deck = _und.shuffle(this.deck);
  },

  createHand: function(num_cards) {
    if (this.deck.length < 5)
      console.log('DECK IS LESS THAN FIVE!:'+this.deck.length);
    for (var i = 0; i < num_cards; i++)
      this.hand.push(this.deck.shift() );
    
    return this.hand;
  },

  // using dispose() as a name instead of discard() because
  // it looks like discard() is causing conflicts with Mongoose
  disposeCards: function(num_cards) {
    for (var i = 0; i < num_cards; i++) {
      this.discard.push(this.hand.shift());
    }
    return this.discard;
  },



  // returns the last token
  getToken: function() {
    return this.turnTokens[this.turnTokens.length-1];
  }

};

playerStatusSchema.statics = {
  getSchema: function() {
    return this.playerStatusSchema;
  }
};

module.exports = mongoose.model('PlayerStatus', playerStatusSchema);