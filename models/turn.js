var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var turnSchema = new Schema({
  token:    String,
  buys:     Number,
  actions:  Number,
  game_id:  String,
  user_id:  String,
});

turnSchema.statics = {
  getSchema: function() {
    return this.turnSchema;
  }
};

module.exports = mongoose.model('Turn', turnSchema);
