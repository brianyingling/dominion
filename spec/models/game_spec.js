var Game         = require('../../models/game.js');
var User         = require('../../models/user.js');
var PlayerStatus = require('../../models/playerStatus.js');

describe('Game', function() {
  
  var game = new Game();
  beforeEach(function() {
    game.title         = 'Test Game Title';
    game.maxNumPlayers = 4;
  });

  describe('properties', function() {
    it('has a game title', function() {
      expect(game.title).toEqual('Test Game Title');
    });
    it('has a max number of players', function() {
      expect(game.maxNumPlayers).toEqual(4);
    });
    it("has a isActive status of 'false' as a default", function() {
      expect(game.isActive).toEqual(false);
    });
    it("is not equal to true by default", function() {
      expect(game.isActive).toNotEqual(true);
    });
    it('is not yet won by default', function() {
      expect(game.isWon).toEqual(false);
      expect(game.isWon).toNotEqual(true);
    });
    it('does not yet have a winner', function() {
      expect(game.winner).toBeNull();
    });
    it('does not yet have a turn token', function() {
      expect(game.turnToken).toBeNull();
    });
  });

  describe('.createTurnToken', function() {
    it('creates a randomly generated turn token for a PlayerStatus to use', function() {
      var len = 10;
      token = game.createTurnToken(len);
      expect(token.length).toEqual(len);
    });
  });

  describe('.players', function() {
    it('contains an array of players', function() {
      var player = new PlayerStatus();
      game.players.push(player);
      expect(game.players.length).toEqual(1);
      expect(game.players[0]).toEqual(player);
    });
  });

  describe('.assignTurnToken', function() {
    it('assigns the generated token to a playerStatus object', function() {
      var player, token;
      player = new PlayerStatus();
      token  = game.createTurnToken(10);
      
      game.players.push(player);
      game.assignTurnToken(token, player);
      expect(player.turnTokens[0]).toEqual(token);
    });
  });

});
