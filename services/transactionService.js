
var GameService = require('./gameService');

function TransactionService() {}

// determines whether the the treasure cards value equals the worth
// of the chosen card to purchase
// @money = array of cards used to purchase
// @purchase = card to buy
// @buys - the number of buys a player has
// returns true or false
TransactionService.validate = function (money, purchase, buys) {
  var values, sum;

  values = _und.map(money, function(card) {return card.value;});
  sum    = _und.reduce(values, function(memo, num) {return memo + num;}, 0);

  return (purchase.cost === sum) && (buys > 0);
};

// makes the transaction. Card purchase comes from the game and
// returns to the player's discard pile. Treasure cards used to purchase
// the card also go in the discard pile
// @money - array of treasure cards used to purchase
// @purchase - card to buy
// @player - player making the transaction
// returns the updated PlayerStatus object
TransactionService.transact = function (money, purchase, player, game) {
  var i, j;

  // add cards to the player's discard pile
  _und.each(money, function(card) {
    i = player.hand.indexOf(card.name);
    player.discard.push(player.hand.splice(i,1)[0]);
  });

  player.discard.push(purchase);
  
  // remove purchase from the game's table
  GameService.removeCard(game, purchase);
  return player;

};

module.exports = TransactionService;