var PlayerStatusService = function () {};

// verifies whether a select amount of cards exist in 
// a player's hand
// params
// @hand - the player's current hand
// @ids - array of ids of the cards to check to see if they're in the hand
PlayerStatusService.getVerifiedCards = function(hand, ids) {
  return _und.select(hand, function(card) {return ids.indexOf(card.id) > -1;});
};

PlayerStatusService.discardHand = function (player) {
  player.discard.push.apply(player.discard, player.hand);
  player.hand = [];
  return player;
};

PlayerStatusService.restoreDeck = function (player) {
  player.deck.push.apply(player.deck, player.discard);
  player.discard = [];
  player.shuffleDeck();
  return player;
};

// using dispose() as a name instead of discard() because
// it looks like discard() is causing conflicts with Mongoose
PlayerStatusService.disposeCards = function(player, num_cards, callback) {
  for (var i = 0; i < num_cards; i++) {
    this.discard.push(this.hand.shift());
  }

  // return this.discard;
  callback();
};


module.exports = PlayerStatusService;

