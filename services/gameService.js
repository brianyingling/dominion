
var GameService = function() {};


// sends the updated game to each player using sockets
GameService.updatePlayerSockets = function(game, player, io) {
  io.sockets.clients(game.id).forEach(function(socket) {
    var p, gameStatus, updatedPlayer;

    // find the player associated with this socket
    p = _und.select(game.players, function(player){return player.user_id === socket.user.id;})[0];
    
    updatedPlayer = (p.user_id == player.user_id) ? player : p;

    gameStatus = {
      title:       game.title,
      cards:       game.cards,
      isActive:    game.isActive,
      isWon:       game.isWon,
      winner:      game.winner,
      playerNames: _und.map(game.players, function(player){return player.firstName;}),
      player: updatedPlayer
    };

    socket.emit('update',gameStatus);
  });
};

GameService.updatePlayer = function (game, player, idx) {
  game.players[idx] = player;
  game.markModified('players');
  game.save(function (err) {
    if (err) return err;
  });
};

// determines whether the token passed to it matches the game's token
GameService.verifyToken = function(game, token) {
  return token === game.turnTokens[game.turnTokens.length-1];
};

// removes a card from the table
GameService.removeCard = function(game, card) {
  var c, i;

  c = _und.select(game.cards, function(c) {return card.name == c.name;})[0];
  i = game.cards.indexOf(c);
  game.cards.splice(i,1);
  game.save(function (err) {if (err) return err;});
};


module.exports = GameService;
